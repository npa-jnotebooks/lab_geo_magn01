import math
from math import exp, cos, pi, sqrt
import matplotlib.pyplot as plt

def npa_CC_generic_functions(lambda1=2, omega1=10, lambda2=2, omega2=90, shift=10):
    
    CC = 0.0
    C1 = 0.0
    C2 = 0.0
    
    x_all = []
    f1_all = []
    f2_all = []
    
    o1=omega1/pi
    o2=omega2/pi
    
    x=0
    i=-100
    while i < 100:
        x=i/10
        x_all.append(x)
        f1= exp(-1.0*lambda1*abs(x)) * cos(lambda1*o1*abs(x))
        f1_all.append(f1)
        f2= 0.1 * exp(-1.0*lambda2*abs(x+shift)) * cos(lambda2*o2*abs(x+shift))
        f2_all.append(f2)
        CC = CC + f1*f2
        C1 = C1 + f1**2
        C2 = C2 + f2**2
        i+=1
        
        
    CC = CC/(sqrt(C1)*sqrt(C2))



    plt.figure(figsize=(15,4))
    
    plt.plot(x_all,f1_all, color='blue')
    plt.plot(x_all,f2_all, color='red')
    
    plt.ylabel('Amplitude')
    plt.xlim(-10,10)
    plt.ylim(-1,1)
    plt.xlabel('X')
    
    print('\nCROSS-CORRELAZIONE TRA LE CURVE: ', CC)
    
    return

