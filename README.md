# LAB_GEO_MAGN01

Jupyter Notebook per studiare la magnetizzazione residua al ridge mid-oceanico

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_magn01/HEAD?labpath=UNIMIB_GEO_MAGN01_eltanin19.ipynb)
